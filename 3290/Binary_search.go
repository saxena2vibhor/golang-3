package main
import "fmt"
func main(){
	fmt.Printf("Enter size of your array: ")
	var size int
	fmt.Scanln(&size)
	var arr = make([]int, size)
	for i:=0; i<size; i++ {
		fmt.Printf("Enter %dth element: ", i)
		fmt.Scanf("%d", &arr[i])
	}
	fmt.Println("Your array is: ", arr)
	fmt.Println("Enter Element to be searched: ")
	var ele int
	fmt.Scanln(&ele)
	binarySearch(arr, ele)
	fmt.Println("Your element has been searched")
}
func binarySearch(arr []int, ele int) int {
	var lo int = 0
	var hi int = len(arr) - 1
  
	for lo <= hi {
	  var mid int = lo + (hi-lo)/2
	  var midValue int = arr[mid]
	  fmt.Println("Middle value is:", midValue)
  
	  if midValue == ele {
		return mid
	  } else if midValue > ele {
		// We want to use the left half of our list
		hi = mid - 1
	  } else {
		// We want to use the right half of our list
		lo = mid + 1
	  }
	}
  
	// If we get here we tried to look at an invalid sub-list
	// which means the number isn't in our list.
	return -1
  }
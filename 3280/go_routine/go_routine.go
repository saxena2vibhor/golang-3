// implementing go rountine

package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func run() {

	wg.Add(3)
	go go_routine()
	go go_routine()
	go go_routine()

	fmt.Println("Main done")
	wg.Wait()
}

func go_routine() {
	fmt.Println("Hi")
	time.Sleep(1 * time.Second)
	wg.Done()
}

func main() {
	run()
}

/// This program is for binary search, if "num" is found then return index
///otherwise return -1

package main

import "fmt"

func binarySearch(key int, array []int) int {
	low := 0
	high := len(array) - 1

	for low <= high {
		mid := low + (high-low)/2
		if array[mid] > key {
			high = mid - 1
		} else if array[mid] < key {
			low = mid + 1
		} else {
			return mid
		}
	}
	return -1
}

func main() {
	items := []int{1, 2, 9, 14, 32, 56, 64, 72, 105}
	fmt.Println(binarySearch(15, items))
}

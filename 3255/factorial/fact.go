// this program is for factorial for number "n"

package main

import "fmt"

func main() {
	var n int
	var factorial int
	factorial = 1
	fmt.Print("Enter number = ")
	fmt.Scanln(&n)
	for i := 1; i <= n; i++ {
		factorial = factorial * i
	}
	fmt.Println("factorial of ", n, " = ", factorial)
}

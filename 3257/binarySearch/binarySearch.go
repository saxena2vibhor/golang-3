package main

import "fmt"

func binarySearch(list []int, target int) bool {

	var start = 0
	var end = len(list) - 1
	for start <= end {
		mid := (start + end) / 2
		if list[mid] == target {
			return true
		} else if list[mid] > target {
			end = mid - 1
		} else {
			start = mid + 1
		}
	}
	return false
}
func main() {

	var list = []int{5, 11, 30, 36, 45, 59, 69, 75, 81, 90}
	fmt.Println(binarySearch(list, 36))
}

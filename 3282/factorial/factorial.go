/*

Determing factorial of a number using recursive algorithm

*/

package main
import (
	"fmt"
)

func factorial(n int) int{
	if n==0 || n==1 {
		return n
	}

	return n*factorial(n-1);
}

func main(){
	var n int
	fmt.Print("Enter number: ")
	fmt.Scanln(&n)

	var factorialOfN=factorial(n);
	fmt.Println("Factorial of ",n," is: ",factorialOfN);
}
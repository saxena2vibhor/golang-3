package main

import "fmt"

func main() {
	var slice1 = []int{2,3,5,2,6,1,4}
	ans := sum(slice1)
	fmt.Println(ans)
}

func sum(s []int) int{
	var ans = 0
	for i:=0;i<len(s);i++ {
		ans += s[i]
	}
	
	return ans
}
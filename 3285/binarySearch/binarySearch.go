
// Below program searches 41 value in an array using binarySearch and if found then displays True


package main

import "fmt"

func binarySearch(target int, items []int) int {

    low := 0
    high := len(items) - 1

    for low <= high {
        median := low + (high - low) / 2

        if items[median] > target {
            high = median - 1
        } else if items[median] < target {
            low = median + 1
        } else {
            return median
        }
    
       
    }

    // if low == len(items) || items[low] != target {
    //     return false
    // }

    // return true
   
    return -1

}

func main() {
    items := []int{1, 2, 7, 27, 37, 41, 69, 90, 1010}
    var ans int = binarySearch(78, items)
    if ans != -1 {
        fmt.Println("Found at index " , ans)
    } else{
        fmt.Println("Element do not exist")
    }
}

// learning traversel on array and comparing two arrays
package main

import "fmt"

func main() {
	array1 := [5]int{1, 2, 3}
	array2 := [5]int{1, 2, 3, 4}

	if array1 == array2 {
		fmt.Print("Equal")
	} else {
		fmt.Println("Not equal")
	}
	fmt.Println(array1)
	fmt.Println(array2)

	for i := 0; i < len(array2); i++ {
		fmt.Printf("%d ", array2[i])
	}
	fmt.Println()
}
